marsni
======

This project is a intended to serve as a testbed for setting up OGC CSW
catalogues based on pycsw.

Installation
------------

These instructions will install pycsw and configure it for using in a testing
environment.

It is run through wsgi with a simple python server and uses sqlite as a
database backend.

1. Install *pycsw* and its dependencies:

   .. code:: bash

      sudo apt-get install python-virtualenv
      mkdir -p ~/dev/marsni
      cd ~/dev/marsni
      virtualenv venv
      source venv/bin/activate
      pip install sqlalchemy pycsw
      mkdir marsni && cd marsni

#. Copy the *csw.wsgi* and *default.cfg* files from pycsw's repository (or use the ones in this repository and edit them to suit your needs)

#. Edit the *default.cfg* file with the desired settings. Don't forget to
   adjust the database location.

#. Use the *pycsw-admin.py* script to setup the database:

   .. code:: bash

      pycsw-admin.py -f default.cfg -c setup_db

#. Launch the catalog with a basic python server and wsgi:

   .. code:: bash

      python csw.wsgi